const mockAxios = require('jest-mock-axios').default;

const {
  filter, baseUrl, getUrl, fetchData,
  crawlKids,
  formatter,
  getLink,
  hasSearchOption,
} = require('../../libs/utils');

afterEach(() => {
  // cleaning up the mess left behind the previous test
  mockAxios.reset();
});

const getData = () => ({
  by: 'websirnik',
  descendants: 284,
  id: 25344358,
  score: 474,
  time: 1607430824,
  title: 'Improving DNS Privacy with Oblivious DoH',
  type: 'story',
  url: 'https://blog.cloudflare.com/oblivious-dns/',
});

const getRes1 = () => ({
  by: 'dang',
  id: 25349541,
  parent: 25344358,
  text: 'All: We changed the URL from <a href="https:&#x2F;&#x2F;techcrunch.com&#x2F;2020&#x2F;12&#x2F;08&#x2F;cloudflare-and-apple-design-a-new-privacy-friendly-internet-protocol&#x2F;" rel="nofollow">https:&#x2F;&#x2F;techcrunch.com&#x2F;2020&#x2F;12&#x2F;08&#x2F;cloudflare-and-apple-desig...</a> to the more detailed source, but you might want to read both.',
  time: 1607452725,
  type: 'comment',
});

const getRes2 = () => ({
  by: 'TrueDuality',
  id: 25346727,
  kids: [
    25347307,
    25350253,
  ],
  parent: 25344358,
  text: 'The biggest and most consistent downside I see with these DNS enhancements is that it prevents filtering at the network level. Querying nameservers is being pushed into applications themselves to support these new features (such as Chrome and Firefox), which bypasses any system resolvers configured on the host. In most cases there is no way to signal from the network that it is not desirable to do this (Firefox being the sole exception). There also is no good way for enterprises to centrally manage these settings. DNS is a major source of information when doing threat hunting on a network and having that go dark is a big problem.<p>Enterprises aside, there has been a rise of people using solutions like pi-hole in their home networks to filter out traffic not just for ads, but known malicious domains, and telemetry trackers (which Apple does get filtered by, only calling them out specifically because they have an active interest in not being filtered like this).<p>Yes I think it&#x27;s also a problem that ISPs are snooping and selling this information, but I think that is a less severe problem than rampant malware infections and the excessive collection of online usage data in the telemetry systems present in every webapp, OS, mobile, or IoT device. This increases privacy in one place, while making it much harder to actively protect yourself from the more aggressive and invasive sources of data collection.',
  time: 1607442335,
  type: 'comment',
});

describe('Test functions in utils.js', () => {
  test('test filter', () => {
    const search = 'changed';
    expect(filter(getRes1(), search)).resolves.toEqual([{
      item: getRes1(),
      refIndex: 0,
    }]);

    expect(filter()).rejects.toThrow('No response to filter');
    expect(filter(getRes1())).rejects.toThrow('No search query to filter');
  });

  test('test getUrl', () => {
    const id = 1234;
    const expected = `${baseUrl + id}.json`;
    expect(getUrl(1234)).toBe(expected);

    expect(() => {
      getUrl();
    }).toThrow('Id is rquired');
  });

  test('test fetchData', () => {
    const url = getUrl(1234);
    const thenFn = jest.fn();
    const catchFn = jest.fn();
    fetchData(url).then(thenFn).catch(catchFn);
    expect(mockAxios.get).toHaveBeenCalledWith(url);

    mockAxios.mockResponse({ data: {} });
    expect(thenFn).toHaveBeenCalledWith({});
    expect(catchFn).not.toHaveBeenCalled();
  });

  test('test crawlKids failure', () => {
    expect.assertions(3);
    const data = getData();
    const res = crawlKids(data);
    expect(res).toBeInstanceOf(Array);
    res.forEach((promise) => {
      promise.catch((e) => {
        expect(e).toBeInstanceOf(Error);
        expect(e.message).toBe('No kids');
      });
    });
  });

  test('test crawlKids success', () => {
    const data = getData();
    data.kids = [25349541, 25346727];
    const thenFn = jest.fn();
    const catchFn = jest.fn();

    // trigger
    crawlKids(data).forEach((promise) => {
      promise.then(thenFn).catch(catchFn);
    });

    // expects kids have crawled
    const url1 = getUrl(25349541);
    const url2 = getUrl(25346727);
    expect(mockAxios.get).toHaveBeenNthCalledWith(1, url1);
    expect(mockAxios.get).toHaveBeenNthCalledWith(2, url2);

    // expect response
    const res1 = getRes1();
    const res2 = getRes2();
    mockAxios.mockResponse({ data: res1 });
    mockAxios.mockResponse({ data: res2 });
    expect(thenFn).toHaveBeenNthCalledWith(1, res1);
    expect(thenFn).toHaveBeenNthCalledWith(2, res2);
  });

  // test formatter function
  test('test formatter', () => {
    const response = getRes1();
    const formatted = {
      link: getLink(25349541),
      text: response.text,
    };
    expect(formatter(response)).toEqual(formatted);
  });

  // test hasSearchOption
  test('test hasSearchOption', () => {
    expect(hasSearchOption({})).toBeFalsy();
    expect(hasSearchOption({ search: 'foo' })).toBeTruthy();
  });
});
