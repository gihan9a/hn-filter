/* eslint-disable no-underscore-dangle */
const axiosMock = require('jest-mock-axios').default;
const app = require('../../index');

const { formatter, getUrl } = require('../../libs/utils');

const mock = jest.createMockFromModule('../../libs/utils');
mock.printData = jest.fn((fmt, data) => data);

afterEach(() => {
  // cleaning up the mess left behind the previous test
  axiosMock.reset();
  mock.printData.mockReset();
});

const getArgs = () => ({
  item: 25355099,
});
const getResponse = () => ({
  data: {
    by: 'wavelander',
    descendants: 4,
    id: 25355099,
    kids: [25356716, 25356100],
    score: 51,
    time: 1607485755,
    title: 'Stanford MLSys Seminar Series',
    type: 'story',
    url: 'https://mlsys.stanford.edu/?ref=mlnews',
  },
});

describe('Test app', () => {
  test('test error', () => {
    expect(() => app({})).toThrow('Id is rquired');
  });

  const testFirstCall = (args) => {
    const promise = app(args, mock.printData);
    const url1 = getUrl(args.item);
    expect(axiosMock.get).toHaveBeenNthCalledWith(1, url1);
    const response = getResponse();
    axiosMock.mockResponse(response);
    return { response, promise };
  };

  test('test without search parameter', async () => {
    const args = getArgs();
    const { response, promise } = testFirstCall(args);
    await promise;
    expect(mock.printData).toHaveBeenCalledWith(formatter, response.data);
  });

  test('test with search parameter', async () => {
    const args = getArgs();
    args.search = 'Uber';

    const { response, promise } = testFirstCall(args);
    const url2 = getUrl(response.data.kids[0]);
    expect(axiosMock.get).toHaveBeenNthCalledWith(2, url2);
    const url3 = getUrl(response.data.kids[1]);
    expect(axiosMock.get).toHaveBeenNthCalledWith(3, url3);
    const response1 = {
      data: {
        by: 'spicyramen',
        id: 25356716,
        kids: [25357588],
        parent: 25355099,
        text: 'Piero and Matel from Uber and Databricks definetly worth listening to',
        time: 1607504859,
        type: 'comment',
      },
    };
    const response2 = {
      data: {
        by: 'SOMA_BOFH',
        id: 25356100,
        kids: [25357574],
        parent: 25355099,
        text: 'no sign-up without a gmail account?  c&#x27;mon man!',
        time: 1607497646,
        type: 'comment',
      },
    };
    axiosMock.mockResponse(response1);
    axiosMock.mockResponse(response2);

    await promise;
    expect(mock.printData).toHaveBeenNthCalledWith(1, formatter, response1.data);
  });
});
