# HN filter

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/46c93e80d0f2430991ea83fe53da183a)](https://www.codacy.com/gl/gihan9a/hn-filter/dashboard?utm_source=gitlab.com&utm_medium=referral&utm_content=gihan9a/hn-filter&utm_campaign=Badge_Grade)

A Simple Hacker News Filter based on fuzzy search

<!-- TABLE OF CONTENTS -->

<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#motivation">Motivation</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

## Motivation

Sometimes Hacker News threads contain huge number of comments. So it's difficult to focus on what we need.
This tools helps to filter particular discussion based on your filters

### Built with

-   [Node.js](https://nodejs.org/en/)
-   [Axios](https://github.com/axios/axios/)
-   [Fuse.js](https://fusejs.io/)
-   [Yargs](http://yargs.js.org/)

## Getting Started

### Prerequisites

This is an npm cli tool. So you must have following prerequisites in your system.

-   Node.js >= 12

### Installation

You can install this package via npm registry.

Following command will install globaly in your system.

`npm i -g hn-filter`

**Note**: you might want to use `sudo` for the above command.

Or if you are just checking temporary use `npx` utility

`npx hn-filter [options]`

## Usage

`hn-filter -i <item> --search "<query>"` 

## Contributing

Contributions are welcome :heart:

## License

This work is licensed under the terms of the MIT license.  
For a copy, see <https://opensource.org/licenses/MIT>.

## Contact

Twitter <https://twitter.com/gihan9a>  
Gitter <https://gitter.im/gihan9a>
