/* eslint-disable no-console */
const {
  getUrl,
  fetchData,
  crawlKids,
  filter,
  formatter,
  hasSearchOption,
} = require('./libs/utils');

module.exports = function App(args, printData) {
  return fetchData(getUrl(args.item))
    .then((data) => {
      if (!hasSearchOption(args)) {
        return printData(formatter, data);
      }
      const promises = crawlKids(data);
      return promises.forEach((promise) => {
        promise
          .then((info) => filter(info, args.search))
          .then((info) => {
            if (info.length > 0) {
              printData(formatter, info[0].item);
            }
          })
          .catch((err) => console.error(err));
      });
    })
    .catch((err) => {
      console.error(err);
    });
};
