#!/usr/bin/env node
/* eslint-disable no-console */
const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers');
const chalk = require('chalk');
const { printData } = require('./libs/utils');

const app = require('./index');

// parse args
const { argv } = yargs(hideBin(process.argv))
  .usage('$0 <cmd> [args]')
  .option('item', {
    alias: 'i',
    describe: 'Item id of the HN discussion',
  })
  .option('search', {
    alias: 's',
    describe: 'Search keywords',
  })
  .option('limit', {
    alias: 'l',
    describe: 'Limit output',
  })
  .demandOption(['item'], 'Please provide item id of the discussion')
  .help();

(async () => {
  console.log(chalk.yellow('Starting...'));
  await app(argv, printData);
  console.log(chalk.green('Done!'));
})();
