/* eslint-disable no-console */
const Fuse = require('fuse.js');
const axios = require('axios').default;
const chalk = require('chalk');

const baseUrl = 'https://hacker-news.firebaseio.com/v0/item/';
module.exports.baseUrl = baseUrl;

const baseLink = 'https://news.ycombinator.com/item';
module.exports.baseLink = baseLink;

/**
 * Get API url encpoint of the Hacker News
 *
 * @param {number} id Id of the hacker news item
 *
 * @returns {string}
 *
 * @author Gihan S <gihanshp@gmail.com>
 */
module.exports.getUrl = (id) => {
  if (id === undefined) {
    throw new Error('Id is rquired');
  }
  return `${baseUrl}${id}.json`;
};

/**
 * Get Link of the hacker news web
 *
 * @param {number} id Id of the hacker news item
 *
 * @returns {string}
 *
 * @author Gihan S <gihanshp@gmail.com>
 */
module.exports.getLink = (id) => {
  if (id === undefined) {
    throw new Error('Id is rquired');
  }
  return `${baseLink}?id=${id}`;
};

/**
 * Filter response with search string
 *
 * @param {object} res Response object
 * @param {string} search Search query string
 *
 * @returns {Promise}
 *
 * @author Gihan S <gihanshp@gmail.com>
 */
module.exports.filter = (res, search) => {
  if (!res) {
    return Promise.reject(new Error('No response to filter'));
  }
  if (!search) {
    return Promise.reject(new Error('No search query to filter'));
  }
  const fuse = new Fuse([res], {
    keys: ['text'],
  });
  return Promise.resolve(fuse.search(search));
};

/**
 * Fetch data from the given API endpoint
 *
 * @param {string} url getUrl
 *
 * @returns {Promise}
 *
 * @author Gihan S <gihanshp@gmail.com>
 */
module.exports.fetchData = (url) => axios.get(url)
  .then((res) => res.data)
  .catch((error) => console.error(error));

/**
   * Print data into stdout
   *
   * @param {function} formatter Formatter function
   * @param {object} data data to format and print
   *
   * @returns {void}
   *
   * @author Gihan S <gihanshp@gmail.com>
   */
module.exports.printData = (formatter, data) => {
  const formatted = formatter(data);
  console.group(chalk.blueBright(formatted.link));
  console.log(formatted.text);
  console.groupEnd(formatted.link);
  console.log('');
};

/**
 * Crawl Hacker News kids
 *
 * @param {object} data Data object
 *
 * @returns {object} Array object containing promises
 *
 * @author Gihan S <gihanshp@gmail.com>
 */
module.exports.crawlKids = (data) => {
  if (!data.kids || data.kids.length === 0) {
    return [Promise.reject(new Error('No kids'))];
  }
  return data.kids.map((kid) => this.fetchData(this.getUrl(kid)));
};

/**
 * Format the given data
 *
 * @param {data} data Data to format
 *
 * @returns {objct} Formatted object
 *
 * @author Gihan S <gihanshp@gmail.com>
 */
module.exports.formatter = (data) => {
  let text = data;
  if (data.text) {
    text = data.text;
  } else if (data.title) {
    text = data.title;
  }
  return {
    link: this.getLink(data.id),
    text,
  };
};

/**
 * Has provided search argument?
 *
 * @param {object} args
 *
 * @returns {boolean}
 *
 * @author Gihan S <gihanshp@gmail.com>
 */
module.exports.hasSearchOption = (args) => {
  if (!args.search || args.search.trim() === '') {
    return false;
  }
  return true;
};
